import 'package:flutter/foundation.dart';

class Umkm {
  final String id;
  final List<String> domisili;
  final String merekbisnis;
  final String logousahaUrl;
  final String gambarusahaUrl;
  final String produkjasa;
  final String sahamumkm;
  final String pendanaandibutuhkan;
  final String deskripsi;

  const Umkm({
    @required this.id,
    @required this.domisili,
    @required this.merekbisnis,
    @required this.logousahaUrl,
    @required this.gambarusahaUrl,
    @required this.produkjasa,
    @required this.sahamumkm,
    @required this.pendanaandibutuhkan,
    @required this.deskripsi,
  });
}
