import 'package:flutter/material.dart';

import '../widgets/umkm_item.dart';
import '../models/umkm.dart';

class CategoryUmkmScreen extends StatefulWidget {
  static const routeName = '/category-umkm';

  final List<Umkm> availableUmkm;

  CategoryUmkmScreen(this.availableUmkm);

  @override
  _CategoryUmkmScreenState createState() => _CategoryUmkmScreenState();
}

class _CategoryUmkmScreenState extends State<CategoryUmkmScreen> {
  String categoryTitle;
  List<Umkm> displayedUmkm;
  var _loadedInitData = false;

  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final domisiliId = routeArgs['id'];
      displayedUmkm = widget.availableUmkm.where((umkm) {
        return umkm.domisili.contains(domisiliId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeUmkm(String umkmId) {
    setState(() {
      displayedUmkm.removeWhere((umkm) => umkm.id == umkmId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return UmkmItem(
            id: displayedUmkm[index].id,
            merekbisnis: displayedUmkm[index].merekbisnis,
            logousahaUrl: displayedUmkm[index].logousahaUrl,
            deskripsi: displayedUmkm[index].deskripsi,
          );
        },
        itemCount: displayedUmkm.length,
      ),
    );
  }
}
