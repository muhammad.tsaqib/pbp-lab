import 'package:flutter/material.dart';

import '../dummy_data.dart';

class UmkmDetailScreen extends StatelessWidget {
  static const routeName = '/umkm-detail';

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 150,
      width: 300,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final umkmId = ModalRoute.of(context).settings.arguments as String;
    final selectedUmkm = DUMMY_UMKM.firstWhere((umkm) => umkm.id == umkmId);
    return Scaffold(
      appBar: AppBar(
        title: Text('${selectedUmkm.merekbisnis}'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                selectedUmkm.logousahaUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildSectionTitle(context, 'Deskripsi UMKM'),
            buildContainer(
              ListView.builder(
                itemBuilder: (ctx, index) => Card(
                      color: Theme.of(context).accentColor,
                      child: Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 5,
                            horizontal: 10,
                          ),
                          child: Text(selectedUmkm.deskripsi)),
                    ),
              ),
            ),
            buildSectionTitle(context, 'Saham UMKM'),
            buildContainer(
              ListView.builder(
                itemBuilder: (ctx, index) => Card(
                  color: Theme.of(context).accentColor,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 10,
                      ),
                      child: Text(selectedUmkm.sahamumkm)),
                ),
              ),
            ),
            buildSectionTitle(context, 'Pendanaan Dibutuhkan UMKM'),
            buildContainer(
              ListView.builder(
                itemBuilder: (ctx, index) => Card(
                  color: Theme.of(context).accentColor,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 10,
                      ),
                      child: Text(selectedUmkm.pendanaandibutuhkan)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
