import 'package:flutter/material.dart';

import './models/category.dart';
import './models/umkm.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Aceh',
    color: Colors.purple,
  ),
  Category(
    id: 'c2',
    title: 'Sumatera Utara',
    color: Colors.red,
  ),
  Category(
    id: 'c3',
    title: 'Sumatera Barat',
    color: Colors.orange,
  ),
  Category(
    id: 'c4',
    title: 'Riau',
    color: Colors.amber,
  ),
  Category(
    id: 'c5',
    title: 'Kepulauan Riau',
    color: Colors.blue,
  ),
  Category(
    id: 'c6',
    title: 'Jambi',
    color: Colors.green,
  ),
  Category(
    id: 'c7',
    title: 'Sumatera Selatan',
    color: Colors.lightBlue,
  ),
  Category(
    id: 'c8',
    title: 'Kepulauan Bangka Belitung',
    color: Colors.lightGreen,
  ),
  Category(
    id: 'c9',
    title: 'Bengkulu',
    color: Colors.pink,
  ),
  Category(
    id: 'c10',
    title: 'Lampung',
    color: Colors.teal,
  ),
  Category(
    id: 'c11',
    title: 'DKI Jakarta',
    color: Colors.greenAccent,
  ),
  Category(
    id: 'c12',
    title: 'Banten',
    color: Colors.red,
  ),
  Category(
    id: 'c13',
    title: 'Jawa Barat',
    color: Colors.lightGreenAccent,
  ),
  Category(
    id: 'c14',
    title: 'Jawa Tengah',
    color: Colors.amberAccent,
  ),
  Category(
    id: 'c15',
    title: 'Jawa Timur',
    color: Colors.blue,
  ),
];

const DUMMY_UMKM = const [
  Umkm(
    id: 'u1',
    domisili: [
      'c1',
      'c15',
    ],
    merekbisnis: 'Alfamart',
    logousahaUrl:
        'https://vestnorden.com/wp-content/uploads/2018/03/house-placeholder.png',
    produkjasa: 'Kebutuhan Rumah Tangga',
    sahamumkm: '120000000',
    pendanaandibutuhkan: '500000000',
    deskripsi: 'Toko yang menyediakan alat rumah tangga',
    gambarusahaUrl:
        'https://vestnorden.com/wp-content/uploads/2018/03/house-placeholder.png',
  ),
];