from django.http import response
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Friend
from .forms import FriendForm


# Create your views here.
@login_required(login_url='/admin/login/') 

def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

def add_friend(request):
    form = FriendForm()
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-3/')
        
    context = {'form':form}
    return render(request, 'lab3_form.html', context)
