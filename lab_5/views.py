from django.shortcuts import render
from datetime import datetime, date
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)