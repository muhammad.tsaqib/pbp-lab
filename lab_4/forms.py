from django import forms
from django.db.models import fields
from .models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
