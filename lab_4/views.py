from django.shortcuts import render, redirect
from datetime import datetime, date
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
from .forms import NoteForm

def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-4/')
        
    context = {'form':form}
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)