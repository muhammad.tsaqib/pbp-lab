### Apakah perbedaan antara JSON dan XML?

Perbedaan yang paling menonjol menurut saya adalah data pada JSON disimpan dalam bentuk key dan value, 
berbasis object Java Script, dan JSON didesain menjadi self-describing sehingga JSON lebih mudah untuk dibaca dan dimengerti.
Pada XML, semua data yang ingin disajikan perlu dimulai dengan start dan end tag dan semua tag bersifat case sensitive.
Hal ini membuat sintaks dari XML lebih strict daripada JSON. XML juga memiliki kelebihan dibandingkan JSON yaitu XML
bisa mensupport berbagai tipe encoding, sedangkan JSON hanya mensupport encoding UTF-8.

### Apakah perbedaan antara HTML dan XML?

HTML dan XML memiliki format yang sama yaitu keduanya menggunakan berbagai tag untuk menampilkan elemen pada website. Namun,
perbedaannya terletak di bagian tag yaitu pada HTML, semua tag tidak case-sensitive sedangkan tag pada XML bersifat case-sensitive.
Pada HTML juga tidak diharuskan memiliki end tag seperti penggunaan < br>, sedangkan semua tag pada XML diharuskan menggunakan end tag.
